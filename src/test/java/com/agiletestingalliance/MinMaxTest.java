package com.agiletestingalliance;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MinMaxTest {
    @Test
    public void testFireball() {
        MinMax minMax = new MinMax();
        int result = minMax.fireball(3, 4);
        assertEquals(4, result);
    }

    @Test
    public void testBar() {
        MinMax minMax = new MinMax();
        String result = minMax.bar("Hello World!");
        assertEquals("Hello World!", result);
    }
}
